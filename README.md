# What is Overseas?
Overseas is search engine for finding links you cannot usually find.

# I can't access the links?
You can search for unblockers and connection IPs with Overseas.

# Will Overseas go down?
If Overseas goes down, we will move to a new domain.

# How do I know which domain you will move to?
Email us at overseas-domains@mt2015.com - saying you want to be notified when we move. I will respond when we move.
If you don't get an email, go to mt2015.com or myTrashMail.com and access my email.
There you can see who did get the email and what it said. You can also send emails out.

# I'm trusting you all not to abuse access to my email - please send out emails to help others or look for new domains
# No, we do not have a Reddit page but please post anything on the email by sending an email to overseas-sendmailto@mt2015.com
# Thank you